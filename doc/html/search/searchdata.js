var indexSectionsWithContent =
{
  0: "abcdfghilmnrst",
  1: "adhmrs",
  2: "b",
  3: "adhmrs",
  4: "bcdgilmnt",
  5: "rs",
  6: "fgn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties"
};

