﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace BusInfoWPF
{
    /// <summary>
    /// Logika pobierania informacji z bazy 
    /// </summary>
    class DataAccess
    {
        /// <summary>
        /// Zapytanie które pobiera informacje po numerze linii 
        /// </summary>
        /// <param name="nr_linii"></param>
        /// <returns></returns>
        public List<Robocze> GetRobocze(string nr_linii)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Robocze>($"SELECT * FROM Robocze WHERE Nr_linii like'{nr_linii}'").ToList();
                return output;
            }
        }

        /// <summary>
        /// Zapytanie które pobiera wszystkie informacje na temat linii 
        /// </summary>
        /// <returns></returns>
        public List<Robocze> GetRozklad()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Robocze>($"SELECT nr_linii, nazwa_przystanku, godzina_odjazdu FROM ROBOCZE;").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie które pobiera informacje według numeru linii 
        /// </summary>
        /// <param name="nr_linii"></param>
        /// <returns></returns>
        public List<Soboty> GetSoboty(string nr_linii)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Soboty>($"SELECT * FROM Soboty WHERE Nr_linii like'{nr_linii}'").ToList();
                return output;
            }
        }

        /// <summary>
        /// Zapytanie które pobiera informacje o linii dla Soboty 
        /// </summary>
        /// <returns></returns>
        public List<Soboty> GetRozkladSob()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Soboty>($"SELECT nr_linii, nazwa_przystanku, godzina_odjazdu FROM SOBOTY;").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie które pobiera informacje o numerze linii dla świąt
        /// </summary>
        /// <param name="nr_linii"></param>
        /// <returns></returns>
        public List<Swieta> GetSwieta(string nr_linii)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Swieta>($"SELECT * FROM Swieta WHERE Nr_linii like'{nr_linii}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie które pobiera informacje z tabeli święta 
        /// </summary>
        /// <returns></returns>
        public List<Swieta> GetSwietaOg()
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Swieta>($"SELECT nr_linii, nazwa_przystanku, godzina_odjazdu FROM Swieta;").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie wyświetlające informacje o nazwie przystanku z tabeli robocze
        /// </summary>
        /// <param name="nazwa_przystanku"></param>
        /// <returns></returns>
        public List<Robocze> GetStopRob(string nazwa_przystanku)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Robocze>($"SELECT * FROM Robocze WHERE nazwa_przystanku like'{nazwa_przystanku}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie wyświetlające informacje o nazwie przystanku z tabeli soboty
        /// </summary>
        /// <param name="nazwa_przystanku"></param>
        /// <returns></returns>
        public List<Soboty> GetStopSob(string nazwa_przystanku)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Soboty>($"SELECT * FROM Soboty WHERE nazwa_przystanku like'{nazwa_przystanku}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie wyświetlające informacje o nazwie przystanku z tabeli święta
        /// </summary>
        /// <param name="nazwa_przystanku"></param>
        /// <returns></returns>
        public List<Swieta> GetStopSw(string nazwa_przystanku)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Swieta>($"SELECT * FROM Swieta WHERE nazwa_przystanku like'{nazwa_przystanku}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie które wyświetla posortowane godziny odjazdu z tabeli robocze 
        /// </summary>
        /// <param name="godzina_odjazdu"></param>
        /// <returns></returns>
        public List<Robocze> GetHourRob(string godzina_odjazdu)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Robocze>($"SELECT * FROM Robocze WHERE godzina_odjazdu <='{godzina_odjazdu}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie które wyświetla posortowane godziny odjazdu z tabeli soboty
        /// </summary>
        /// <param name="godzina_odjazdu"></param>
        /// <returns></returns>
        public List<Soboty> GetHourSob(string godzina_odjazdu)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Soboty>($"SELECT * FROM Soboty WHERE godzina_odjazdu <='{godzina_odjazdu}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Zapytanie które wyświetla posortowane godziny odjazdu z tabeli święta
        /// </summary>
        /// <param name="godzina_odjazdu"></param>
        /// <returns></returns>
        public List<Swieta> GetHourSw(string godzina_odjazdu)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                var output = connection.Query<Swieta>($"SELECT * FROM Soboty WHERE godzina_odjazdu <='{godzina_odjazdu}'").ToList();
                return output;
            }
        }
        /// <summary>
        /// Dodawanie informacji o liniach autobusowych
        /// </summary>
        /// <param name="nrLinii"></param>
        /// <param name="nazwaPrzystanku"></param>
        /// <param name="godzinaOdjazdu"></param>
        public void InsertNewLine(string nrLinii, string nazwaPrzystanku, string godzinaOdjazdu)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("BUS")))
            {
                Robocze newRobocze = new Robocze {  nr_linii = nrLinii,  nazwa_przystanku = nazwaPrzystanku, godzina_odjazdu = godzinaOdjazdu };
                
                List<Robocze> robocze = new List<Robocze>();

                robocze.Add(newRobocze);
                connection.Execute("dbo.Robocze_Insert @nr_linii, @nazwa_przystanku, @godzina_odjazdu", robocze);
            }
        }


    }
}
