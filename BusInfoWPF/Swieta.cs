﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusInfoWPF
{
    class Swieta
    {
        public string nr_linii { get; set; }
        public string nazwa_przystanku { get; set; }
        public string godzina_odjazdu { get; set; }

        public string FullInfo3
        {
            get
            {
                //opis pól
                return $"{nr_linii} {nazwa_przystanku} {godzina_odjazdu}";
            }
        }
    }
}
