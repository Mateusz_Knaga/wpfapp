﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BusInfoWPF
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Tworzenie nowych list
        /// </summary>
        List<Robocze> robocze = new List<Robocze>();
        List<Soboty> soboty = new List<Soboty>();
        List<Swieta> swieta = new List<Swieta>();

        public MainWindow()
        {
            InitializeComponent();
            roboczeFoundListbox.ItemsSource = robocze;
            roboczeFoundListbox.DisplayMemberPath = "FullInfo";

            sobotyFoundListbox.ItemsSource = soboty;
            sobotyFoundListbox.DisplayMemberPath = "FullInfo2";

            swietaFoundListbox.ItemsSource = swieta;
            swietaFoundListbox.DisplayMemberPath = "FullInfo3";
        }

        /// <summary>
        /// Wyświetlanie informacji według numeru linii 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataAccess db = new DataAccess();

            robocze = db.GetRobocze(nrLiniiText.Text);
            roboczeFoundListbox.ItemsSource = robocze;
            roboczeFoundListbox.DisplayMemberPath = "FullInfo";

            soboty = db.GetSoboty(nrLiniiText.Text);
            sobotyFoundListbox.ItemsSource = soboty;
            sobotyFoundListbox.DisplayMemberPath = "FullInfo2";

            swieta = db.GetSwieta(nrLiniiText.Text);
            swietaFoundListbox.ItemsSource = swieta;
            swietaFoundListbox.DisplayMemberPath = "FullInfo3";

        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Wyświetlanie wszystkich informacji 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DataAccess db = new DataAccess();
            robocze = db.GetRozklad();
            roboczeFoundListbox.ItemsSource = robocze;
            roboczeFoundListbox.DisplayMemberPath = "FullInfo";

            soboty = db.GetRozkladSob();
            sobotyFoundListbox.ItemsSource = soboty;
            sobotyFoundListbox.DisplayMemberPath = "FullInfo2";

            swieta = db.GetSwietaOg();
            swietaFoundListbox.ItemsSource = swieta;
            swietaFoundListbox.DisplayMemberPath = "FullInfo3";
        }

        /// <summary>
        /// Wyszukiwanie po nazwie przystanku 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DataAccess db = new DataAccess();

            robocze = db.GetStopRob(nazwaPrzystankuText.Text);
            roboczeFoundListbox.ItemsSource = robocze;
            roboczeFoundListbox.DisplayMemberPath = "FullInfo";

            soboty = db.GetStopSob(nazwaPrzystankuText.Text);
            sobotyFoundListbox.ItemsSource = soboty;
            sobotyFoundListbox.DisplayMemberPath = "FullInfo2";

            swieta = db.GetStopSw(nazwaPrzystankuText.Text);
            swietaFoundListbox.ItemsSource = swieta;
            swietaFoundListbox.DisplayMemberPath = "FullInfo3";

        }

        private void nrLiniiText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //DataAccess db = new DataAccess();

            //db.InsertNewLine(nrInsText.Text, nazwaInsText.Text, godzinaInsText.Text);

            //nrInsText.Text = "";
            //nazwaInsText.Text = "";
            //godzinaInsText.Text = "";
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        /// <summary>
        /// Wyświetlanie według godzin 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            DataAccess db = new DataAccess();
            robocze = db.GetHourRob(hourInsText.Text);
            roboczeFoundListbox.ItemsSource = robocze;
            roboczeFoundListbox.DisplayMemberPath = "FullInfo";

            soboty = db.GetHourSob(hourInsText.Text);
            sobotyFoundListbox.ItemsSource = soboty;
            sobotyFoundListbox.DisplayMemberPath = "FullInfo2";

            swieta = db.GetHourSw(hourInsText.Text);
            swietaFoundListbox.ItemsSource = swieta;
            swietaFoundListbox.DisplayMemberPath = "FullInfo3";
        }
    }
}
